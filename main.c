// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = ON       // Power-up Timer Enable (PWRT enabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Memory Code Protection (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = ON        // Internal/External Switchover (Internal/External Switchover mode is enabled)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is enabled)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config VCAPEN = OFF     // Voltage Regulator Capacitor Enable (All VCAP pin functionality is disabled)
#pragma config PLLEN = ON       // PLL Enable (4x PLL enabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LVP = ON         // Low-Voltage Programming Enable (Low-voltage programming enabled)


#include <xc.h>
#define _XTAL_FREQ 32000000
#include<stdio.h>
#include"flex_lcd.h"
#include<string.h>
#include <xc.h>


//FUNCTIONS
void encabezado();
char digitoasci(char);
void enviartemp(float);
float leertemp();


//VARIABLES
int adc;
float volt,temp;
unsigned char buffer1[16];
int estadoencabezado;


//CONSTANTS
#define resolution 1024.0
#define vpp 5.0


//FUNCTION MAIN
void main(void) {
    //oscillator 32MHz
    OSCCONbits.IRCF=0b1110;//Select clock 8MHZ
    OSCCONbits.SCS=0b00;//Config1 INTOSC
    //PORTS
    ANSELA=0b00100000;//RA5 SELECT AS ANALOGIC
    TRISA=0b00100000;//RA5 CONFIGURE AS IN
    //ADC
    ADCON0bits.CHS=4;              //SELECT THE CHANEL 4
    ADCON1bits.ADNREF=0;         //reference negative=Vss
    ADCON1bits.ADPREF=0;        //reference positive=Vdd
    ADCON1bits.ADCS=0b111;       //FRECUENCY CONVERSION
    ADCON1bits.ADFM=1;              //JUSTIFIED
    ADCON0bits.ADON=1;            //ADC TURN ON
    //FOR UART COMMUNICATION
    TRISC=0b10000000;
    PORTC=0;
    //CONFIGURATE SERIAL PORT
    //baud rate
    TXSTAbits.SYNC=0;//asyncronous transmission
    TXSTAbits.TXEN=1;//turn on transmission
    RCSTAbits.SPEN=1;//turn on serial port
    //**********BAUD RATE
    BAUDCONbits.BRG16=0;// 8 bits
    TXSTAbits.BRGH=0;// low speed
    SPBRG=51;        
   //LCD
    Lcd_Init();       //inicializamos el LCD
    Lcd_Cmd(LCD_CLEAR);  //limpiamos LCD
    Lcd_Cmd(LCD_CURSOR_OFF);//APAGAMOS EL CURSOR
    __delay_ms(100); //esperamos 100ms
    //SEND ENCABEZADO ONLY ONE TIME
    int noenviado;
    noenviado=1;
    estadoencabezado=1;
    
    
    while(1){
        if(estadoencabezado==noenviado)encabezado();
        leertemp();
        enviartemp(temp);
    }
    
    return;
}

//FUNCTION FOR SEND THE WORD:"TEMPERATURE"
void encabezado()
{
            TXREG=84;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            TXREG=69;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            TXREG=77;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            TXREG=80;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            TXREG=69;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            TXREG=82;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            TXREG=65;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            TXREG=84;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            TXREG=85;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            TXREG=82;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            TXREG=65;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            TXREG=32;
            while(TXSTAbits.TRMT==0);
            __delay_ms(500);
            estadoencabezado=2;
            
}


//FUNCTION TO READ TEMPERATURE
float leertemp()
{
        ADCON0bits.GO_nDONE=1;    //START THE CONVERSION
        while(ADCON0bits.GO_nDONE);
        adc=ADRESH;
        adc=adc<<8;
        adc=adc+ADRESL;
        volt=adc*vpp/resolution;
        sprintf(buffer1,"ADC %04d",adc);
        Lcd_Out2(1,1,buffer1);
        temp=volt*100;
        sprintf(buffer1,"Temp %04.2f",temp);
        Lcd_Out2(2,1,buffer1);
        __delay_ms(300);
        return temp;
}


//FUNCTION FOR SEND THE TEMPERATURE
void enviartemp(float temperat)
{
    char String[7];
    sprintf(String,"%1.2f",temperat);
    int i;
    for(i=0;i<(strlen(String)-1);i++)
    {
        char send;
        send=digitoasci(String[i]);
        TXREG=send;
        while(TXSTAbits.TRMT==0);
        __delay_ms(500);
    }
    TXREG=32;
    while(TXSTAbits.TRMT==0);
    __delay_ms(500);
    }


//FUNCTION DIGIT TO ASCI
char digitoasci( char digito)
{
    
  switch (digito)
  {
      case'0':
          return 48;
      break;
      case'1':
          return 49;
      break;
      case'2':
          return 50;
      break;
      case'3':
          return 51;
      break;
      case'4':
          return 52;
      break;
      case'5':
          return 53;
      break;
      case'6':
          return 54;
      break;
      case'7':
          return 55;
      break;
      case'8':
          return 56;
      break;
      case'9':
          return 57;
      break;
      case'.':
          return 46;
      break;
  }
}
